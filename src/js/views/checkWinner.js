export default function(active, UIstrings) {
    let row1col1, row1col2, row1col3, row2col1, row2col2, row2col3, row3col1, row3col2, row3col3, url, row1, row2, row3, col1, col2, col3, backSlash, slash;
    row1col1 = document.querySelector(UIstrings.rowCol11).getAttribute('src');
    row1col2 = document.querySelector(UIstrings.rowCol12).getAttribute('src');
    row1col3 = document.querySelector(UIstrings.rowCol13).getAttribute('src');
    row2col1 = document.querySelector(UIstrings.rowCol21).getAttribute('src');
    row2col2 = document.querySelector(UIstrings.rowCol22).getAttribute('src');
    row2col3 = document.querySelector(UIstrings.rowCol23).getAttribute('src');
    row3col1 = document.querySelector(UIstrings.rowCol31).getAttribute('src');
    row3col2 = document.querySelector(UIstrings.rowCol32).getAttribute('src');
    row3col3 = document.querySelector(UIstrings.rowCol33).getAttribute('src');
    
    url = 'img/' + active + '.png';
    row1 = (row1col1 === url && row1col2 === url && row1col3 === url);
    row2 = (row2col1 === url && row2col2 === url && row2col3 === url);
    row3 = (row3col1 === url && row3col2 === url && row3col3 === url);
    col1 = (row1col1 === url && row2col1 === url && row3col1 === url);
    col2 = (row1col2 === url && row2col2 === url && row3col2 === url);
    col3 = (row1col3 === url && row2col3 === url && row3col3 === url);
    backSlash = (row1col1 === url && row2col2 === url && row3col3 === url);
    slash = (row1col3 === url && row2col2 === url && row3col1 === url);
    if (row1 || row2 || row3 || col1 || col2 || col3 || slash || backSlash) {
        return true;
    } else {
        return false;
    }
};