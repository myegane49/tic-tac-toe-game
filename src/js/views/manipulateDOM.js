export const resetTable = function(UIstrings) {
    let tdList = document.querySelectorAll(UIstrings.xo);
    for (let cur of tdList) {
        cur.setAttribute('src', '');
    }
};
export const updatePanels = function(data, UIstrings) {
    document.querySelector(UIstrings.p1total).textContent = `Total wins = ${data.player1.score}`;
    document.querySelector(UIstrings.p2total).textContent = `Total wins = ${data.player2.score}`;
    document.querySelector(UIstrings.p1active).classList = `${data.player1.activation}`;
    document.querySelector(UIstrings.p2active).classList = `${data.player2.activation}`;
};
export const winnerPanel = function(data, UIstrings) {
    if (data.player1.activation === 'green') {
        document.querySelector(UIstrings.p1Title).textContent = 'Player 1 Wins!';
        document.querySelector(UIstrings.p1Title).classList.add('golden');
    } else if (data.player2.activation === 'green') {
        document.querySelector(UIstrings.p2Title).textContent = 'Player 2 Wins!';
        document.querySelector(UIstrings.p2Title).classList.add('golden');
    }
};
export const clearWinnerPanel = function(data, UIstrings) {
    if (data.player2.activation === 'green') {
        document.querySelector(UIstrings.p2Title).textContent = 'Player 2';
        document.querySelector(UIstrings.p2Title).classList.remove('golden');
    } else if (data.player1.activation === 'green') {
        document.querySelector(UIstrings.p1Title).textContent = 'Player 1';
        document.querySelector(UIstrings.p1Title).classList.remove('golden');
    }
};