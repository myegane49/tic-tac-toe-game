import data from './models/Data';
import updateData from './models/DataManipulate';
import UIstrings from './views/uiStrings';
import * as UICtrl from './views/manipulateDOM';
import checkWinner from './views/checkWinner';

let playingState;
const resetGame = function() {
    // let beginData = {player1: {score: 0, activation: 'green'}, player2: {score: 0, activation: 'red'}};
    playingState = true;
    updateData(data, 0, 'green', 0, 'red');
    UICtrl.updatePanels(data, UIstrings);
    UICtrl.resetTable(UIstrings);
    UICtrl.clearWinnerPanel(data, UIstrings);
    setupEventListeners();
};
resetGame();

function setupEventListeners() {
    document.querySelector('.reset-game').addEventListener('click', resetGame);
    document.querySelector('.reset-table').addEventListener('click', () => {
        UICtrl.resetTable(UIstrings);
        UICtrl.clearWinnerPanel(data, UIstrings);
        if (data.player1.activation === 'green') {
            updateData(data, data.player1.score, 'red', data.player2.score, 'green');
        } else if (data.player2.activation === 'green') {
            updateData(data, data.player1.score, 'green', data.player2.score, 'red');
        }
        UICtrl.updatePanels(data, UIstrings);
        playingState = true;
    });
    document.querySelector('table').addEventListener('click', event => {
        if (playingState === true) {
            if (event.target.className === UIstrings.tdClasses && data.player1.activation === 'green' && event.target.firstChild.getAttribute('src') === '') {
                // event.target.firstChild.style.display = 'inline-block';
                event.target.firstChild.src = "img/o.png";
                if (checkWinner('o', UIstrings)) {
                    UICtrl.winnerPanel(data, UIstrings);
                    updateData(data, data.player1.score + 1, 'green', data.player2.score, 'red');
                    UICtrl.updatePanels(data, UIstrings);
                    playingState = false;
                    // UICtrl.resetTable();
                } else {
                    // if (UICtrl.tabelFull()) {
                    //     UICtrl.resetTable();
                    // }
                    updateData(data, data.player1.score, 'red', data.player2.score, 'green');
                    UICtrl.clearWinnerPanel(data, UIstrings);
                    UICtrl.updatePanels(data, UIstrings);
                }
            } else if (event.target.className === UIstrings.tdClasses && data.player2.activation === 'green' && event.target.firstChild.getAttribute('src') === '') {
                // event.target.firstChild.style.display = 'inline-block';
                event.target.firstChild.src = "img/x.png";
                if (checkWinner('x', UIstrings)) {
                    UICtrl.winnerPanel(data, UIstrings);
                    updateData(data, data.player1.score, 'red', data.player2.score + 1, 'green');
                    UICtrl.updatePanels(data, UIstrings);
                    playingState = false;
                    // UICtrl.resetTable();
                } else {
                    // if (UICtrl.tabelFull()) {
                    //     UICtrl.resetTable();
                    // }
                    updateData(data, data.player1.score, 'green', data.player2.score, 'red');
                    UICtrl.clearWinnerPanel(data, UIstrings);
                    UICtrl.updatePanels(data, UIstrings);
                }
            }
        }
    });  
}










