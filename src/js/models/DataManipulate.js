export default function(data, p1Score, p1State, p2Score, p2State) {
    data.player1.score = p1Score;
    data.player2.score = p2Score;
    data.player1.activation = p1State;
    data.player2.activation = p2State;
};